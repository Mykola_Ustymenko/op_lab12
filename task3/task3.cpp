﻿// task3.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <stdio.h>
#include <windows.h>


int main()
{
    SetConsoleCP(1251); SetConsoleOutputCP(1251);
    int n;
    printf("Введіть розмір масиву: \n");
    scanf_s("%d", &n);
    int* arr = (int*)malloc(n * sizeof(int));
    printf("Введіть елементи масиву:\n");
    for (int i=0; i < n; ++i) {
        scanf_s("%d", &arr[i]);
    }
    for (int i = 0; i < n; ++i) {
        for (int j = i + 1; j < n;) {
            if (arr[i] == arr[j]) {
                for (int k = j; k < n - 1; ++k) {
                    arr[k] = arr[k + 1];
                }n--;
            }
            else ++j;
        }
    }
    printf("\nТепер масив виглядає так:\n");
    for (int i = 0; i < n; ++i) {
        printf("%d ", arr[i]);
    }
    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
