﻿// task2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include<windows.h>
#include<stdio.h>

int main()
{
    SetConsoleCP(1251); SetConsoleOutputCP(1251);
    int na, nb, nc;
    printf("Введіть розмір масиву А:\n");
    scanf_s("%d", &na);
    printf("Введіть розмір масиву B:\n");
    scanf_s("%d", &nb);
    printf("Введіть розмір масиву C:\n");
    scanf_s("%d", &nc);
    int* a = (int*)malloc(na * sizeof(int));
    int* b = (int*)malloc(nb * sizeof(int));
    int* c = (int*)malloc(nc * sizeof(int));
    printf("Введіть елементи масиву А:\n");
    for (int i = 0; i < na; i++) {
        scanf_s("%d", &a[i]);
    }
    printf("Введіть елементи масиву B:\n");
    for (int i = 0; i < nb; i++) {
        scanf_s("%d", &b[i]);
    }
    printf("Введіть елементи масиву C:\n");
    for (int i = 0; i < nc; i++) {
        scanf_s("%d", &c[i]);
    }
    int nd = na + nb + nc;
    int* d = (int*)malloc(nd * sizeof(int));
    int i = 0, j = 0, k = 0;
    while (i < na || j < nb || k < nc) {
        int valA = (i < na) ? a[i] : INT_MIN;
        int valB = (j < nb) ? b[j] : INT_MIN;
        int valC = (k < nc) ? c[k] : INT_MIN;
        if (valA >= valB && valA >= valC) {
            d[i + j + k] = valA;
            i++;
        }
        else if (valB >= valA && valB > valC) {
            d[i + j + k] = valB;
            j++;
        }
        else {
            d[i + j + k] = valC;
            k++;
        }
    }
    printf("Впорядкований масив D:\n");
    for (int l = 0; l < nd; l++) {
        printf("%d ", d[l]);
    }
    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
