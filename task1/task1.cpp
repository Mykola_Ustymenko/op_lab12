﻿// task1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <stdio.h>
#include <Windows.h>
#include <limits.h>
#include<math.h>


int main()
{
    SetConsoleCP(1251); SetConsoleOutputCP(1251);
    int n;
    printf("Введіть розмір масиву:\n");
    scanf_s("%d", &n);
    if (n < 3) {
        printf("Кількість елементів має бути більше 2\n");
        return 1;
    }
    int* arr = (int*)malloc(n * sizeof(int));
    printf("Введіть елементи масиву:\n");
    for (int i = 0; i < n; i++) {
        scanf_s("%d", &arr[i]);
    }
    int ind1 = 0, ind2 = 0;
    int minDiff = INT_MAX;
    for (int i = 0; i < n - 1; ++i) {
        int diff = abs(arr[i] - arr[i + 1]);
        if (diff < minDiff) {
            minDiff = diff;
            ind1 = i;
            ind2 = i + 2;
        }
    }
    printf("Номера найближчих елементів: %d, %d", ind1, ind2);
    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
